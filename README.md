# WEEK CYCLE BACKUP #

Powershell script creates 7z backup archives with current weekday digit in the end of file name.  
Folders from backup.txt files are included to backup archive.

### How do I get set up? ###

* Place folder 1 in disk C: in order for example configuration file to work

### Configuration ###

* Amend backup.txt file
* first line specifies backup destination path
* the rest lines specify paths that will be included in archive