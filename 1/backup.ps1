﻿$ScriptPath = Split-Path -parent $PSCommandPath
$ConfigPath = $ScriptPath + "\backup.txt"

if (!(Test-Path $ConfigPath)) {
    Write-Host "Configuration file $ConfigPath not found."
    Exit
}

$Paths = ""
$Text = Get-Content -Path $ConfigPath

for ($i=1; $i -lt $Text.Length; $i++) {
  $Paths = $Paths + " " + $Text[$i]
}

$ArchivePath = $Text[0]

$Paths = $Paths.Substring(1)

$ZipFile = $ScriptPath + "\7za.exe"

$WeekDay = (Get-Date).DayOfWeek.value__
$ArchiveFileName = "backup$WeekDay.7z"

$ArchivePath = $ArchivePath + "\" + $ArchiveFileName

if (Test-Path $ArchivePath) {
    Remove-Item $ArchivePath
}

Start-Process -FilePath $ZipFile -ArgumentList "a","-t7z",$ArchivePath,$Paths -WorkingDirectory $ScriptPath -WindowStyle hidden
